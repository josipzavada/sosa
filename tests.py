import unittest
import math
from dodatak import OperationsManager

class OperationsManagerTests(unittest.TestCase):

    def test_int_division(self):
        opm = OperationsManager(6, 2)
        result = opm.perform_division()
        self.assertEqual(result, 3)

    def test_float_division(self):
        opm = OperationsManager(6.5, 2)
        result = opm.perform_division()
        self.assertEqual(result, 3.25)

    def test_division_with_zero(self):
        opm = OperationsManager(5, 0)
        result = opm.perform_division()
        self.assertTrue(math.isnan(result))


if __name__ == '__main__':
    unittest.main()
